<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSwapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swaps', function (Blueprint $table) {
            $table->id();
            $table->foreignId('player_id')->constrained()->cascadeOnDelete();
            $table->foreignId('old_value_id')->constrained('values')->cascadeOnDelete();
            $table->foreignId('new_value_id')->constrained('values')->cascadeOnDelete();
            $table->foreignId('advisor_id')->nullable()->constrained('players')->onDelete('set null');
            $table->boolean('is_approved')->nullable();
            $table->boolean('is_applied');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swaps');
    }
}
