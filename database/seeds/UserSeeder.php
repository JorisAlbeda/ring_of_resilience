<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'name' => 'admin',
            'email' => 'contact@godenspel.nl',
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('deringofderuit')
        ]);
    }
}
