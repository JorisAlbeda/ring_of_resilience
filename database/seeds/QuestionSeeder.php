<?php

use App\Question;
use App\Value;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->delete();
        DB::table('answers')->delete();
        DB::table('answer_value')->delete();

        $question_file = File::get(storage_path("seeding_data/Vragen.txt"));
        $question_file_clean = str_replace("\r", "", $question_file);
        $question_list = explode("\n", $question_file_clean);

        $i = 0;

        while($i < count($question_list)) {
            $q_text = $question_list[$i];

            if($q_text[0] == '<') { //Text
                $q_text = str_replace('<', '', $q_text);
                $paragraph_nl = "";

                while(substr($q_text, -1) != '>') {
                    $paragraph_nl = $paragraph_nl . $q_text . "\n";
                    $i++;
                    $q_text = $question_list[$i];
                }
                $paragraph_nl = $paragraph_nl . "\n" . str_replace('>', '', $q_text);

                $i++;
                $q_text = str_replace('<', '', $question_list[$i]);
                $paragraph_en = "";

                while(substr($q_text, -1) != '>') {
                    $paragraph_en = $paragraph_en . $q_text . "\n";
                    $i++;
                    $q_text = $question_list[$i];
                }
                $paragraph_en = $paragraph_en . "\n" . str_replace('>', '', $q_text);

                Question::create([
                    "body" => [
                        'nl' => $paragraph_nl,
                        'en' => $paragraph_en,
                    ],
                    "is_question" => false
                ]);

            } else { //Question
                $i++;
                $q_text_en = $question_list[$i];

                $i++;
                $yes_text = $question_list[$i];
                $yes_values = explode(",", strtolower(str_replace(" ", "", $yes_text)));

                $i++;
                $no_text = $question_list[$i];
                $no_values = explode(",", strtolower(str_replace(" ", "", $no_text)));

                $q = Question::create([
                    "body" => [
                        'nl' => $q_text,
                        'en' => $q_text_en,
                    ]
                ]);

                $yes = $q->answers()->create([
                    "body" => [
                        'nl' => "Ja",
                        'en' => "Yes",
                    ]
                ]);
                foreach($yes_values as $value_name) {
                    //filthy hack because fuck you, it's a seeder
                    if($value_name == 'vrijvangeest') {
                        $value_name = 'vrij van geest';
                    }
                    $yes->values()->attach(Value::where("name", $value_name)->get());
                }

                $no = $q->answers()->create([
                    "body" => [
                        'nl' => "Nee",
                        'en' => "No",
                    ]
                ]);
                foreach($no_values as $value_name) {
                    //filthy hack because fuck you, it's a seeder
                    if($value_name == 'vrijvangeest') {
                        $value_name = 'vrij van geest';
                    }
                    $no->values()->attach(Value::where("name", $value_name)->get());
                }
            }

            $i++;
        }
    }
}
