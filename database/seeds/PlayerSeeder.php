<?php

use Illuminate\Database\Seeder;
use App\Player;
use Carbon\Carbon;
use App\Session;
use App\Value;

class PlayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $persons_file = File::get(storage_path("seeding_data/persons.txt"));
        $persons_file_clean = str_replace("\r", "", $persons_file);
        $persons_list = explode("\n", $persons_file_clean);

        $i = 0;

        $session = new Session();
        $session->timestamps = false;
        $session->created_at = Carbon::createFromDate(2017, 05, 20);
        $session->updated_at = Carbon::now();
        $session->save();

        while($i < count($persons_list)) {
            $name = $persons_list[$i];
            $i++;
            $value_bit_string = $persons_list[$i];

            $player = new Player();
            $player->name = $name;
            $player->session_id = $session->id;
            $player->timestamps = false;
            $player->created_at = Carbon::createFromDate(2017, 05, 20);
            $player->updated_at = Carbon::now();
            $player->save();

            $values = ["dwaas","onschuldig","gehoorzaam","slim","bescheiden","liefdevol","flexibel","mooi","machtig","hedendaags",
            			"overmoedig","verbindend","revolutionair","utopisch","onbetrouwbaar","wetsgetrouw","huwelijksgetrouw",
            			"rechtvaardig","jeugdig","tijdloos","voorzichtig","respectvol","bewust","geil","kwaadaardig","huichelachtig",
            			"onderdanig","driftig","kapitalistisch","watervlug","speels","geslachtloos","daadkrachtig","oersterk",
            			"vrij van geest","oprecht","aards","afhankelijk","trots","vurig","nostalgisch","listig","gastvrij",
            			"beïnvloedbaar","eerlijk","hoopvol","bejaard","anarchistisch","helderziend"]; //Old order.

            foreach(str_split($value_bit_string) as $index => $has_value) {
                if($has_value) {
                    $value_name = $values[$index];
                    $value = Value::where('name', $value_name)->get();
                    $player->values()->attach($value);
                }
            }

            $i++;
        }
    }
}
