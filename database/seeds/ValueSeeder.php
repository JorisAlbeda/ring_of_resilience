<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Value;

class ValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('values')->delete();

        /*
         * Begin top 8: onschuldig, gehoorzaam, slim, bescheiden, liefdevol, flexibel, mooi, machtig
         * Eind top 8: liefdevol, slim, vrij van geest, verbindend, flexibel, respectvol, hoopvol, gastvrij
         */

        $values = ["liefdevol","slim","vrij van geest","onschuldig","verbindend","flexibel","respectvol","hoopvol","gastvrij", //TOP ACHT
            "gehoorzaam","bescheiden","mooi","machtig","hedendaags",
            "overmoedig","revolutionair","utopisch","onbetrouwbaar","wetsgetrouw","huwelijksgetrouw",
            "rechtvaardig","jeugdig","tijdloos","voorzichtig","bewust","geil","kwaadaardig","huichelachtig",
            "onderdanig","driftig","kapitalistisch","watervlug","speels","geslachtloos","daadkrachtig","oersterk",
            "oprecht","aards","afhankelijk","trots","vurig","nostalgisch","listig",
            "beïnvloedbaar","eerlijk","bejaard","anarchistisch","helderziend", "dwaas"]; //TODO: Fix ï.

        foreach($values as $i => $value) {
            Value::create([
                'name' => $value,
                'position' => $i + 1 <= 8 ? $i + 1 : null,
            ]);
        }
    }
}
