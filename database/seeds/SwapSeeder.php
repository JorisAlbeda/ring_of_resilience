<?php

use Illuminate\Database\Seeder;
use App\Player;
use App\Swap;
use App\Value;
use Carbon\Carbon;

class SwapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $swaps_file = File::get(storage_path("seeding_data/swaps.txt"));
        $swaps_file_clean = str_replace("\r", "", $swaps_file);
        $swaps_list = explode("\n", $swaps_file_clean);

        $i = 0;

        while($i < count($swaps_list)) {
            $player_name = $swaps_list[$i];
            $player = Player::where('name', $player_name)->first();
            $i++;
            $swap_data = explode(' ', $swaps_list[$i]);
            $old_v_index = $swap_data[0];
            $new_v_index = $swap_data[1];
            $time = $swap_data[2];

            $values = ["dwaas","onschuldig","gehoorzaam","slim","bescheiden","liefdevol","flexibel","mooi","machtig","hedendaags",
                        "overmoedig","verbindend","revolutionair","utopisch","onbetrouwbaar","wetsgetrouw","huwelijksgetrouw",
                        "rechtvaardig","jeugdig","tijdloos","voorzichtig","respectvol","bewust","geil","kwaadaardig","huichelachtig",
                        "onderdanig","driftig","kapitalistisch","watervlug","speels","geslachtloos","daadkrachtig","oersterk",
                        "vrij van geest","oprecht","aards","afhankelijk","trots","vurig","nostalgisch","listig","gastvrij",
                        "beïnvloedbaar","eerlijk","hoopvol","bejaard","anarchistisch","helderziend"]; //Old order.

            $old_value = Value::where('name', $values[$old_v_index])->first();
            $new_value = Value::where('name', $values[$new_v_index])->first();

            $swap = new Swap();
            $swap->timestamps = false;
            $swap->fill([
                 'player_id' => $player->id,
                 'old_value_id' => $old_value->id,
                 'new_value_id' => $new_value->id,
                 'notes' => '',
                 'is_approved' => true,
                 'is_applied' => true,
                 'created_at' => Carbon::create(date('Y-m-d H:i:s', $time/1000)),
                 'updated_at' => Carbon::now(),
             ]);
             $swap->save();
            $i++;
        }
    }
}
