<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'OverviewController@index');
Route::get('/about', 'OverviewController@about');
Route::get('/photos', 'OverviewController@photos');
Route::get('/testimonials', 'OverviewController@testimonials');
Route::get('/contact', 'OverviewController@contact');
Route::get('/disclaimer', 'OverviewController@disclaimer');

Route::group([
    'middleware' => 'auth',
    'as' => 'admin.',
    'prefix' => 'admin',
], function() {

    Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);
    Route::resource('photos', 'PhotoController');
    Route::resource('testimonials', 'TestimonialController');
    Route::resource('swaps', 'SwapController');

    Route::group([
        'as' => 'game.',
        'prefix' => 'game',
    ], function() {
        Route::get('/{locale?}', ['as' => 'start', 'uses' => 'SessionController@start']);

        Route::get('/top-eight', 'ValueController@getTopEight');

        Route::post('players', 'PlayerController@store');

        Route::post('sessions/{session_id}', 'SessionController@saveAll');

        Route::get('sessions/restart', 'SessionController@restart');
    });

});

Auth::routes(['register' => false]);

