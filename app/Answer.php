<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasTranslations;

class Answer extends Model
{
    use HasTranslations;

    public $translatable = ['body'];

    public function values() {
        return self::belongsToMany(Value::class);
    }
}
