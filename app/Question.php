<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasTranslations;

class Question extends Model
{
    use HasTranslations;

    public $translatable = ['body'];

    public function answers() {
        return self::hasMany(Answer::class);
    }
}
