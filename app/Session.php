<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        'is_finished'
    ];

    public function players() {
        return $this->hasMany(Player::class);
    }

    public static function getNewSessionId() {
        $newSession = self::create();
        return $newSession->id;
    }

    public static function getData() {
        $newId = self::getNewSessionId();
        return [
            'questions' => Question::with('answers.values')->get(),
            'session_id' => $newId,
            'values' => Value::orderBy('position', 'DESC')->get(),
        ];
    }

    public function saveAll(array $data) {
        foreach($data['players'] as $index => $player_data){
            $player = Player::create([
                'name' => $player_data['name'],
                'session_id' => $this->id,
            ]);

            //Save the id in the player's array so the Swap can use it.
            $data['players'][$index]['id'] = $player->id;

            foreach($player_data['values'] as $value){
                $player->values()->attach($value['id']);
            }

            foreach($player_data['answers'] as $answer){
                $player->answers()->attach($answer['id']);
            }
        }

        foreach($data['judges'] as $judge_data){
            $judge = Judge::create([
                'name' => $judge_data['name'],
                'session_id' => $this->id,
            ]);
        }

        foreach($data['swaps'] as $swap_data){
            $swap_player_index = $swap_data['player']['index'];
            $swap_player = $data['players'][$swap_player_index];

            $swap = Swap::create([
                'player_id' => $swap_player['id'],
                'notes' => array_key_exists('notes', $swap_data) ? $swap_data['notes'] : '',
                'old_value_id' => $swap_data['oldValue']['id'],
                'new_value_id' => $swap_data['newValue']['id'],
                'is_approved' => array_key_exists('is_approved', $swap_data) ? $swap_data['is_approved'] : false,
                'is_applied' => false, //Don't care if the client has applied this one, this flag checks whether the SERVER has applied it.
            ]);

            if($swap->is_approved) {
                $swap->apply();
            }
        }

        $this->update([
            'is_finished' => true
        ]);
    }
}
