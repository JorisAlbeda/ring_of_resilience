<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;
use App\Player;
use App\Testimonial;
use App\Value;
use App\Swap;

class OverviewController extends Controller
{
    public function index() {
        $players = Player::orderBy('created_at', 'desc')->get();
        $topEight = Value::getTopEight();
        $wagnerEight = Value::getWagnerEight();
        $swaps = Swap::orderBy('created_at', 'desc')->where('is_applied', true)->get();

        return view('index')
            ->with(compact('players', 'topEight', 'wagnerEight', 'swaps'))
        ;
    }

    public function about() {
        return view('about');
    }

    public function contact() {
        return view('contact');
    }

    public function disclaimer() {
        return view('disclaimer');
    }

    public function photos() {
        return view('photos')
            ->with('photos', Photo::all())
        ;
    }

    public function testimonials() {
        return view('testimonials')
            ->with('testimonials', Testimonial::all())
        ;
    }
}
