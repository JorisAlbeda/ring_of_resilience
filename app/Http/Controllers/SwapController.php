<?php

namespace App\Http\Controllers;

use App\Swap;
use Illuminate\Http\Request;

class SwapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.swaps.index')
            ->with('swaps', Swap::all())
        ;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.swaps.form')
            ->with('swap', new Swap())
        ;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect()->route('admin.swaps.index');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Swap  $swap
     * @return \Illuminate\Http\Response
     */
    public function show(Swap $swap)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Swap  $swap
     * @return \Illuminate\Http\Response
     */
    public function edit(Swap $swap)
    {
        return view('admin.swaps.form')
            ->with('swap', $swap)
        ;
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Swap  $swap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Swap $swap)
    {
        $swap->update([
            'notes' => $request->notes,
        ]);
        return redirect()->route('admin.swaps.index');
    }
}
