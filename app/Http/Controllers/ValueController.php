<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Value;

class ValueController extends Controller
{
    public function getTopEight(){
        $topEight = Value::getTopEight();
        return $topEight;
    }
}
