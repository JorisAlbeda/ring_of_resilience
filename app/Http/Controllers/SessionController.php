<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;

class SessionController extends Controller
{
    public function start(string $locale = null) {
        if(isset($locale)) {
            app()->setLocale($locale);
        }
        return view('admin.game')
            ->with('data', Session::getData());
    }

    public function saveAll(Request $request, int $session_id) {
        $session = Session::find($session_id);
        if($session == null) {
            $session = Session::create(['is_finished' => false]);
        }
        $session->saveAll($request->all());
        return ['message' => 'Data was saved successfully.'];
    }

    public function restart() {
        $newId = Session::getNewSessionId();
        return [
            'message' => 'New session was created successfully.',
            'session_id' => $newId
        ];
    }
}
