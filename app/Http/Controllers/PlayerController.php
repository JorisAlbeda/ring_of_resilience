<?php

namespace App\Http\Controllers;

use App\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        Player::create($request->all());
        
        return ['message' => 'Player ' . $request->name . ' created successfully.'];
    }
}
