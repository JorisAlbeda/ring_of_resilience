<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    protected $fillable = ['position'];
    
    public static function getWagnerEight() {
        return [
            "dwaas",
            "onschuldig",
            "gehoorzaam",
            "slim",
            "bescheiden",
            "liefdevol",
            "flexibel",
            "mooi"
        ];
    }
    
    public static function getTopEight(){
        return self::whereNotNull('position')
            ->orderBy('position')
            ->get();
    }
    
    public static function getTopEightFrom(int $position){
        return self::whereNotNull('position')
            ->where('position', '>', $position)
            ->orderBy('position')
            ->get();
    }
}
