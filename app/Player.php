<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = [
        'name',
        'session_id'
    ];
    
    public function values() {
        return self::belongsToMany(Value::class);
    }
    
    public function answers() {
        return self::belongsToMany(Answer::class);
    }
    
    public function getIsWelcomeAttribute() {
        $topEight = Value::getTopEight();
        
        foreach($topEight as $value) {
            if(!$this->values->contains($value)) {
                return false;
            }
        }
        
        return true;
    }
}
