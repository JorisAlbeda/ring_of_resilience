<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Swap extends Model
{
    protected $fillable = [
        'player_id',
        'old_value_id',
        'new_value_id',
        'notes',
        'is_approved',
        'is_applied',
    ];
    
    public function oldValue() {
        return $this->belongsTo(Value::class, 'old_value_id');
    }
    
    public function newValue() {
        return $this->belongsTo(Value::class, 'new_value_id');
    }
    
    public function player() {
        return $this->belongsTo(Player::class);
    }
    
    public function apply() {
        
        $oldValuePosition = $this->oldValue->position;
        
        if($this->is_approved && isset($oldValuePosition)) {
            $topEight = Value::getTopEightFrom($oldValuePosition);
            
            $this->oldValue->update([
                'position' => null
            ]);
            
            foreach($topEight as $value) {
                $value->update([
                    'position' => $value->position - 1
                ]);
            }
            
            $this->newValue->update([
                'position' => 8
            ]);
            
            $this->update([
                'is_applied' => true
            ]);
            
            return true;
        }
        
        return false;
    }
}
