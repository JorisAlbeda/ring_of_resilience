@extends('layouts.app')
@section('content')

    @include('partials.home-area')

    <div class="container">
        <h1 class="my-4">DISCLAIMER</h1>

        De inwisselbaarheid van de metaforische en letterlijke interpretatie van de waarden van Wagner is overgelaten aan de discretie van het team van <i>The Extincticator</i>.<br>
        Gegeven twee waarden X en Y die door een vraag A in de test als mutueel exclusief zouden worden beschouwd,  bestaat er altijd een vraag B die zorgt dat een kandidaat die in de test voor X is gekwalificeerd, nog voor Y kan kwalificeren OF andersom. In sommige gevallen zijn beide richtingen mogelijk.<br><br>
        Bijvoorbeeld: als een kandidaat in vraag 55 (<i>Kun jij de hitte van de vlammen voelen?</i>) ‘ja’ antwoordt, wordt onder andere de waarde ‘vurig’ toebedeeld. Antwoordt de kandidaat echter ontkennend, dan wordt de waarde ‘watervlug’ toebedeeld. Vragen 9 (<i>Ben jij een kind?</i>) en 18 (<i>Heb jij je wapen al gevonden?</i>) garanderen de mogelijkheid om uiteindelijk voor zowel ‘watervlug’ als ‘vurig’ te kwalificeren.

    </div>

    @include('includes.footer')
@stop
