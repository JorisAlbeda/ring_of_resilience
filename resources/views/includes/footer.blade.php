<footer class="bg-secondary py-2 py-lg-1">
	<div class="container">
		<div class="text-white mt-1">
			<i>"Vaarwel Walhalla, stralende wereld. Verga tot stof."</i>
		</div>
    	<nav class="pt-1 pb-1 navbar navbar-expand navbar-dark">

        	<div class="collapse navbar-collapse" id="mainNavbar">
        		<div class="navbar-nav">
                    <a class="nav-item nav-link" href="/">Home</a>
                    @if(false) {{-- Temporarily disabled until content is available. --}}
                        <a class="nav-item nav-link" href="/testimonials">Testimonials</a>
            		    <a class="nav-item nav-link" href="/photos">Foto's</a>
                    @endif
                    <a class="nav-item nav-link" href="/about">Over het Godenspel</a>
                    <a class="nav-item nav-link" href="/disclaimer">Disclaimer</a>
            		<a class="nav-item nav-link" href="/contact">Contact</a>
                </div>
            </div>
        </nav>
    </div>
</footer>
