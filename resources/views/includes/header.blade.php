<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
    	<a class="navbar-brand" href="{{ route('admin.index') }}">ADMIN</a>
    	<button class="navbar-toggler" type="button" data-toggle="collapse" 
    		data-target="mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Menu openen">
    			<span class="navbar-toggler-icon"></span>	
    	</button>
    	
    	<div class="collapse navbar-collapse" id="mainNavbar">
    		<div class="navbar-nav">
                <a class="nav-item nav-link" href="{{ route('admin.index') }}">Home</a>
                <a class="nav-item nav-link" href="{{ route('admin.testimonials.index') }}">Testimonials</a>
        		<a class="nav-item nav-link" href="{{ route('admin.photos.index') }}">Foto's</a>
        		<a class="nav-item nav-link" href="{{ route('admin.swaps.index') }}">Wissels</a>
            </div>
        </div>
    </nav>
</header>
