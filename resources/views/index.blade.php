@extends('layouts.app')
@section('content')
	<div class="container-fluid archive">
        <div class="title-wrapper d-none d-md-block">
            <div class="title-line">
                @include('partials.title-fluid', [
                    'title' => 'RING*OF*RESILIENCE',
                    'alternate' => 1,
                ])
            </div>
            <div class="title-line">
                @include('partials.title-fluid', [
                    'title' => 'HET******GODENSPEL',
                    'alternate' => 'THE**EXTINCTICATOR',
                ])
            </div>
        </div>

        <div class="title-wrapper d-block d-md-none mb-4">
            <div class="title-line">
                @include('partials.title-fluid', [
                    'title' => '*RING***OF*',
                    'alternate' => 1,
                    ])
                @include('partials.title-fluid', [
                    'title' => 'RESILIENCE*',
                    'alternate' => 1,
                ])
            </div>
            <div class="title-line">
                @include('partials.title-fluid', [
                    'title'     => '****HET****',
                    'alternate' => '****THE****',
                ])
                @include('partials.title-fluid', [
                    'title'     => '*GODENSPEL*',
                    'alternate' => 'XTINCTICATR',
                ])
            </div>
        </div>

    	<div class="my-4">
    	    <i>Het godenspel is een opera-game-installatie over leven, overleven, veerkracht en heldendom.
    	    Neem plaats aan tafel en speel mee voor de laatste overlevenden van een wereldse overstroming.<br>
    	    Gebaseerd op Wagners grootse opera ‘De Ring’, bepalen de deelnemers samen wie mee mag op de boot en wie niet.</i>
        </div>
    	<div class="row">
        	<div class="col-11 col-sm-3 players-col">
        		<h3>Spelers</h3>
    			<p>Lift & Liftrasir</p>
    			<form class="form-inline md-form form-sm mt-0 mb-2">
    			    <i class="fas fa-search" aria-hidden="true"></i>
    			    <input type="text" placeholder="Zoek een naam..." class="form-control form-control-sm ml-3 w-75" id="search" />
                </form>
    			<div class="value-wrapper" id="playerList">
        			@foreach($players as $player)
            			<div class="value" data-name="{{ $player->name }}" data-type="player">
            				<span class="player-name">
            				    {{ $player->name }}
                            </span>
            				<div class="value-result">
            					@if($player->is_welcome)
            						<img class="welcome-icon" src="{{ url('images/welcome.png') }}" />
    							@else
    								<img class="welcome-icon" src="{{ url('images/notwelcome.png') }}" />
    							@endif
            				</div>
        				</div>
            		@endforeach
        		</div>
        	</div>
        	<div class="col-sm-9">
        		<div class="row">
        			<div class="col-sm">
        				<h3>Top 8 Wagner</h3>
        				<p>Siegfried & Brunhilde</p>
            			<ol>
                			@foreach($wagnerEight as $valueName)
                			 	<li>{{ $valueName }}</li>
            			 	@endforeach
        			 	</ol>
        		 	</div>
        		 	<div class="col-sm">
        				<h3>Top 8 Rotterdam</h3>
        				<p>{{ date('d-m-Y') }}</p>
            			<ol>
                			@foreach($topEight as $value)
                			 	<li>{{ $value->name }}</li>
            			 	@endforeach
        			 	</ol>
        		 	</div>
        		 	<div class="col-sm-12 my-3 my-lg-0 col-xl-6">
        		 		<div class="video-wrapper" id="cycler">
                            <img class="active placeholder-photo mr-xs-5" src="{{ url('images/Photos/Photo (1).jpg') }}" alt="The Extincticator" />
                            @for ($i = 2; $i <= 9; $i++)
                                <img class="placeholder-photo mr-xs-5" src="{{ url('images/Photos/Photo (' . $i . ').jpg') }}" alt="The Extincticator {{ $i }}" />
                            @endfor
        		 		</div>
        		 	</div>
    			</div>
        		<div class="row mt-2">
        			<div class="col">
            			<h3>Wissels</h3>
            			<div class="swap-section">
                            @foreach($swaps as $swap)
                                <div class="row mt-4 swap-wrapper" @if(!$loop->first) style="display:none;" @endif>
                                    <div class="col-xl value">
                                        <div class="row w-100">
                                            <div class="col-md d-flex align-items-center">
                                                <span class="value-text">
                                                    {{ $swap->oldValue->name }}
                                                </span>
                                                <i class="fas fa-arrow-right value-arrow d-none d-md-block"></i>
                                            </div>
                                            <div class="col-md d-md-none d-flex justify-content-center">
                                                <i class="fas fa-arrow-down value-arrow ml-0 mr-5 my-3"></i>
                                            </div>
                                            <div class="col-md ml-md-4">
                                                <span class="value-text">
                                                    {{ $swap->newValue->name }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg mt-3 mt-xl-0 d-flex align-items-center">
                                        <div class="row w-100">
                                            <div class="col-md">
                                                {{ $swap->created_at }} door {{ $swap->player->name }}.
                                            </div>
                                            <div class="col-md swap-notes-wrapper">
                                                <i>{{ $swap->notes }}</i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
        			</div>
        		</div>
        		<div class="row mt-5">
        		    <div class="col">
        		        <i>Who's in? Who's out? You decide!</i>
        		    </div>
        		</div>
    		</div>
		</div>
	</div>
	@include('includes.footer')
@stop

@section('js')
    <script type="application/javascript">

        $(function() {
            setInterval(scrollSwaps, 5000);
            scrollPlayerInterval = setInterval(scrollPlayers, 100);
            preloadImages(() => setInterval(cycleImages, 7000));
            /*$('.value-wrapper').scroll(() => clearInterval(scrollPlayerInterval));*/

            initTitleInterval()

            $("#search").on('input', event => {
                let name = event.target.value;

                $("#playerList [data-type='player']").show();
                if(name != "") {
                    $("#playerList [data-type='player']:not([data-name*='" + name + "'])").hide();
                }
            });
        });

        function scrollSwaps() {
            $('.swap-wrapper:first').hide('slide', {direction: 'up'}, function() {
                $('.swap-wrapper').eq(1).show('slide', {direction: 'down'});
                $(this).parent().append(this);
            });
        }

        var playerScrollHeight = 0;
        var scrollPlayerInterval;

        function scrollPlayers() {
            playerScrollHeight++;
            $('.value-wrapper').scrollTop(playerScrollHeight);

            //Check if we've reached the bottom.
            let wrapper = $('.value-wrapper')[0];
            if (wrapper.offsetHeight + wrapper.scrollTop >= wrapper.scrollHeight) {
                setTimeout(() => playerScrollHeight = 0, 2000)
            }
        }

        var titleIndex = 0;
        var letterIndex = 0;
        var titleInterval;

        function initTitleInterval() {
            setTimeout(() => titleInterval = setInterval(switchAllTitles, 100), 10000);
        }

        function switchAllTitles() {
            let chars0 = findCharWrapper(0);
            let chars1 = findCharWrapper(1);
            if(letterIndex < chars1.length ) {
                if(letterIndex < chars0.length) {
                    switchTitle(chars0);
                }
                switchTitle(chars1);
                letterIndex++;
            } else {
                letterIndex = 0;
                titleIndex = 1 - titleIndex;
                clearInterval(titleInterval);
                initTitleInterval();
            }
        }

        function preloadImages(callback) {
            var images = [
                "{{ url('images/Photos/Photo (1).jpg') }}"
                @for ($i = 1; $i <= 9; $i++)
                    , "{{ url('images/Photos/Photo (' . $i . ').jpg') }}"
                @endfor
            ];
            var promises = [];
            for (var i = 0; i < images.length; i++) {
                (function(url, promise) {
                    var img = new Image();
                    img.onload = function() {
                        promise.resolve();
                    };
                    img.src = url;
                })(images[i], promises[i] = $.Deferred());
            }
            $.when.apply($, promises).done(function() {
                callback();
            });
        }

        function cycleImages() {
            var $active = $('#cycler .active');
            var $next = ($active.next().length > 0) ? $active.next() : $('#cycler img:first');
            $next.css('z-index', 2); // Move the next image up the pile
            $active.fadeOut(1500, function() { // fade out the top image
                $active.css('z-index', 1).show().removeClass('active');//reset the z-index and unhide the image
                $next.css('z-index', 3).addClass('active');//make the next image the top one
            });
        }

        function findCharWrapper(deviceIndex) {
            return $('.title-wrapper').eq(deviceIndex)
               .find('.title-line').eq(titleIndex)
               .find('.char-wrapper');
        }

        function switchTitle(chars) {
            chars.eq(letterIndex).find('.original').toggle();
            chars.eq(letterIndex).find('.alternate').toggle();
        }

        function blinkEye() {
            $('#blinkingEye').hide();
            let blinkInterval = setInterval(() => $('#blinkingEye').toggle(), 100);
            setTimeout(() => {
                $('#blinkingEye').show();
                $('#blinkingEye').toggleClass('ml-5');
                $('#blinkingEye').toggleClass('mr-5');
                clearInterval(blinkInterval);
            }, 2000);
        }
    </script>
@stop
