@extends('layouts.app')
@section('content')

    @include('partials.home-area')

    <div class="container">
        <h1 class="my-4">Contact</h1>

        Voor vragen of ideeën, mail <a href="mailto:contact@godenspel.nl">contact@godenspel.nl</a>.
        <br><br>
        Wilt u dat uw naam van deze website wordt verwijderd? Stuur ons gerust een mail.
    </div>

    @include('includes.footer')
@stop
