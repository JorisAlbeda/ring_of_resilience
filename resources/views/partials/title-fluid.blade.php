<div class="row">
    <?php
        if(isset($alternate)) {
            if($alternate == 1) {
                $eyes = [
                    'beyondbeyond',
                    'city',
                    'education',
                    'games',
                    'incubator',
                    'music',
                    'partners',
                    'research',
                    'ring_of_resilience',
                    'songs_of_survival',
                    'sooooon',
                    'theatre'
                ];
                shuffle($eyes);
            } else {
                $alternate_list = str_split($alternate);
            }
        }

    ?>
    @foreach(str_split($title) as $char)
        <div class="col char-wrapper px-0 px-1-md px-lg-3">
            <div class="original">
                @include('partials.title-char', ['char' => $char])
            </div>
            <div class="alternate" style="display:none">
                @if(isset($alternate))
                    @if($alternate == 1)
                        <img class='eye' src='/images/eyes/{{ $eyes[ $loop->index % count($eyes) ] }}.gif'/>
                    @else
                        @include('partials.title-char', [ 'char' => $alternate_list[$loop->index] ])
                    @endif
                @endif
            </div>
        </div>
    @endforeach
</div>
