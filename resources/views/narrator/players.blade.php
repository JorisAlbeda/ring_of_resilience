@extends('layouts.app', ['title' => '1. Intro'])
@section('content')
<div id="root">
	<h2>Spelers</h2>
	<input type="text" id="player_name" v-model="newPlayerName">
	<button v-bind:disabled="isDisabled" class="btn btn-primary" 
			@click="addPlayer" :title="buttonTitle">
		Toevoegen
	</button>
 	<player-list></player-list>	
 	<card title="ring" body="Der Ring des Niebelungen"></card>
</div>
<script>
	var app = new Vue({
		el : "#root",
		data: {
			message: 'Hello World!',
			players: [
				{name:'Sanne', active: false}, 
				{name: 'Joris', active: true}
			],
			buttonTitle: "Speler toevoegen",
			isDisabled: false,
			newPlayerName: "",
		},

		methods: {
			addPlayer() {
				this.players.push({name: this.newPlayerName, active: true});
				this.newPlayerName = "";
				this.isDisabled = true;
			}
		},

		computed: {
			activePlayers() {
				return this.players.filter(player => player.active);
			}
		},

		mounted() {
			
		}
	})
</script>
@stop