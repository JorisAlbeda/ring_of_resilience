@extends('layouts.app')
@section('content')
    @include('partials.home-area')

	<div class="container">
    	<h2>Foto's</h2>
    	<div>
    		@foreach($photos as $photo)
    			<div class="photo-wrapper col-3">
    				<div class="d-block">
    					{{ $photo->name }}
					</div>
    				<img class="img-fluid img-thumbnail" src="{{ asset( Storage::url($photo->url) ) }}" alt="{{ $photo->name }}" title="{{ $photo->name }}">
				</div>
    		@endforeach
    	</div>
	</div>

	@include('includes.footer')
@stop
