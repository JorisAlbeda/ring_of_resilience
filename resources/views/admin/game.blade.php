@extends('layouts.app')
@section('content')
    <ring-header>
    </ring-header>
    
    <div class="container pt-5 game">
    	<router-view 
    		@play-music="playMusic" 
    		@fade-to-music="fadeToMusic" 
    		@pause-music="pauseMusic"
    		@play-sound="playSound"
    		@play="play"
		>
    	</router-view>
    </div>
    
    <footer>
		<audio-player ref="audioPlayer"></ring-audio-player>
	</footer>
@stop