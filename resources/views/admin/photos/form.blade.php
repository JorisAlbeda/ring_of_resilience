@extends('layouts.app')
@section('content')
	@include('includes.header')
	
	<div class="container">
		@if($photo->id == null)
			{{ Form::open(['route' => 'admin.photos.store', 'method' => 'post', 'files' => true]) }}	
		@else
			{{ Form::open(['route' => ['admin.photos.update', $photo->id], 'method' => 'put', 'files' => true]) }}
		@endif
    		<h2>Foto</h2>
    		<div class="form-group">
    			<label for="name">Naam</label>
    			<input class="form-control" type="text" name="name" id="name" value="{{ $photo->name }}" placeholder="Titel van de foto">
			</div>
    		@if(isset($photo))
    			<div class="photo-wrapper col-3">
    				<img src="{{ asset(Storage::url($photo->url)) }}" class="img-fluid" alt="{{ $photo->name }}">
				</div>
    		@endif
    		<div class="custom-file">
    			<input type="file" class="custom-file-input" name="file" id="file">
    			<label class="custom-file-label" for="file">Kies bestand...</label>
			</div>
			<div class="mt-3">
				<button type="submit" class="btn btn-primary">Opslaan</button> <a href="{{ route('admin.photos.index') }}" class="btn">Annuleren</a>
			</div>
		{{ Form::close() }}
	</div>
	
	@include('includes.footer')
@stop