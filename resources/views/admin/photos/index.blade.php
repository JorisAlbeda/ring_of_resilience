@extends('layouts.app')
@section('content')
	@include('includes.header');
	
	<div class="container">
		<div class="row">
        	<div class="col">
        		<h2>Foto's</h2>
    		</div>
    		<div class="col">
    			<a href="{{ route('admin.photos.create') }}">Toevoegen</a>
    		</div>
		</div>
    	<div>
    		@foreach($photos as $photo)
    			<div class="photo-wrapper col-3" id="photo{{ $photo->id }}">
    				<div class="d-block">
    					<a href="{{ route('admin.photos.edit', $photo) }}">{{ $photo->name }}</a> 
    					<a class="float-right text-danger" href="#" onclick="deletePhoto({{ $photo->id }}, '{{ $photo->name }}')">X</a>
					</div>
    				<img class="img-fluid img-thumbnail" src="{{ asset( Storage::url($photo->url) ) }}" alt="{{ $photo->name }}" title="{{ $photo->name }}">
				</div>
    		@endforeach
    	</div>
	</div>
	
	@include('includes.footer')
@stop

@section('js')
	<script>
		function deletePhoto(id, name) {
			if(confirm("Wilt u de foto " + name + " verwijderen?")) {
    			let url = "{{ route('admin.photos.destroy', ':id') }}";
    			console.log(url);
    			url = url.replace(":id", id);
        		axios.delete(url).then(response => {
    				document.querySelector("#photo" + id).style.display = "none";    
    			});
			};
		}
	</script>
@stop