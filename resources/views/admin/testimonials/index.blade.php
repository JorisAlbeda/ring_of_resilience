@extends('layouts.app')
@section('content')
	@include('includes.header');

	<div class="container">
	    <div class="row">
            <div class="col">
                <h2>Testimonials</h2>
            </div>
            <div class="col">
                <a href="{{ route('admin.testimonials.create') }}">Toevoegen</a>
            </div>
        </div>
    	<div>
    		@foreach($testimonials as $testimonial)
    		    <div class="photo-wrapper col-3" id="testimonial{{ $testimonial->id }}">
    		        <div class="d-block">
                        <a href="{{ route('admin.testimonials.edit', $testimonial) }}">{{ $testimonial->name }}</a>
                        <a class="float-right text-danger" href="#" onclick="deleteTestimonial({{ $testimonial->id }}, '{{ $testimonial->name }}')">X</a>
                    </div>
                    <video width="320" height="240" controls>
                        <source src="{{ asset( Storage::url( $testimonial->url ) ) }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
    		@endforeach
    	</div>
	</div>

	@include('includes.footer')
@stop

@section('js')
	<script>
		function deleteTestimonial(id, name) {
			if(confirm("Wilt u de testimonial " + name + " verwijderen?")) {
    			let url = "{{ route('admin.testimonials.destroy', ':id') }}";
    			console.log(url);
    			url = url.replace(":id", id);
        		axios.delete(url).then(response => {
    				$("#testimonial" + id).hide();
    			});
			};
		}
	</script>
@stop
