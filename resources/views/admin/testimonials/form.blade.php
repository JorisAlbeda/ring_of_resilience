@extends('layouts.app')
@section('content')
	@include('includes.header')

	<div class="container">
		@if($testimonial->id == null)
			{{ Form::open(['route' => 'admin.testimonials.store', 'method' => 'post', 'files' => true]) }}
		@else
			{{ Form::open(['route' => ['admin.testimonials.update', $testimonial->id], 'method' => 'put', 'files' => true]) }}
		@endif
    		<h2>Testimonial</h2>
    		<div class="form-group">
    			<label for="name">Naam</label>
    			<input class="form-control" type="text" name="name" id="name" value="{{ $testimonial->name }}" placeholder="Titel van de testimonial">
			</div>
    		@if(isset($testimonial))
    			<video width="320" height="240" controls>
                    <source src="{{ asset( Storage::url( $testimonial->url ) ) }}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
    		@endif
    		<div class="custom-file">
    			<input type="file" class="custom-file-input" name="file" id="file">
    			<label class="custom-file-label" for="file">Kies bestand...</label>
			</div>
			<div class="mt-3">
				<button type="submit" class="btn btn-primary">Opslaan</button> <a href="{{ route('admin.testimonials.index') }}" class="btn">Annuleren</a>
			</div>
		{{ Form::close() }}
	</div>

	@include('includes.footer')
@stop
