@extends('layouts.app')
@section('content')
	@include('includes.header');
	
	<div class="container">
		<p>Welkom in de Admin sectie. Gebruik het menu voor het beheer van testimonials en foto's,
		en om wissels te voorzien van aantekeningen.</p>
		
		<p>Of, om het spel te starten, klik hier:</p>
		
		<a href="{{ route('admin.game.start', 'en') }}" class="btn">Start the game in English</a>
        <a href="{{ route('admin.game.start', 'nl') }}" class="btn btn-primary">Start spel in Nederlands</a>
	</div>
@stop
