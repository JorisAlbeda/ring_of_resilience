@extends('layouts.app')
@section('content')
	@include('includes.header');
	
	<div class="container">
    	<h2>Wissels</h2>
    	<div>
    		@foreach($swaps as $swap)
				<div class="row">
					<div class="col">
						{{ $swap->oldValue->name }} -> {{ $swap->newValue->name }}
					</div>
					<div class="col">
						{{ $swap->player->name }}
					</div>
					<div class="col">
						@if( $swap->is_applied )
							<span class="text-success">Toegepast</span>
						@else
							<span class="text-danger">Afgewezen</span>
						@endif
					</div>
					<div class="col">
						<a href="{{ route('admin.swaps.edit', $swap) }}" class="btn">Bewerken</a>
					</div>
				</div>
    		@endforeach
    	</div>
	</div>
	
	@include('includes.footer')
@stop