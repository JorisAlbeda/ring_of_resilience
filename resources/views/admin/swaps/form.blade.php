@extends('layouts.app')
@section('content')
	@include('includes.header')
	
	<div class="container">
		@if(isset($swap->id))
			{{ Form::open(['route' => ['admin.swaps.update', $swap->id], 'method' => 'put']) }}
        		<h2>Wissel</h2>
        		<div class="row">
    				<div class="col">
    					{{ $swap->oldValue->name }} -> {{ $swap->newValue->name }}
    				</div>
    				<div class="col">
    					{{ $swap->player->name }}
    				</div>
    				<div class="col">
    					@if( $swap->is_applied )
    						<span class="text-success">Toegepast</span>
    					@else
    						<span class="text-danger">Afgewezen</span>
    					@endif
    				</div>
    			</div>
    			
        		<div class="form-group">
        			<label for="notes">Aantekeningen</label>
        			<textarea class="form-control" rows="4" name="notes" id="notes" placeholder="Belangrijkste argument(en)">{{ $swap->notes }}</textarea>
    			</div>
    			
    			<div class="mt-3">
    				<button type="submit" class="btn btn-primary">Opslaan</button> <a href="{{ route('admin.swaps.index') }}" class="btn">Annuleren</a>
    			</div>
    		{{ Form::close() }}
		@else
			<p class="text-danger">Error: wissel niet gevonden. Keer terug naar het <a href="{{ route('admin.swaps.index') }}">menu</a>.</p>
		@endif
	</div>
	
	@include('includes.footer')
@stop