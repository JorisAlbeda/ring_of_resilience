@extends('layouts.app')
@section('content')
    @include('partials.home-area')

	<div class="container">
    	<h2>Testimonials</h2>
    	<div>
    		@foreach($testimonials as $testimonial)
    			<video width="320" height="240" controls>
                	<source src="{{ asset( Storage::url( $testimonial->url ) ) }}" type="video/mp4">
                	Your browser does not support the video tag.
                </video>
    		@endforeach
    	</div>
	</div>

	@include('includes.footer')
@stop
