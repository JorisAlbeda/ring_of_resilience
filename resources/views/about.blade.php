@extends('layouts.app')
@section('content')
    @include('partials.home-area')

    <div class="container">
        <h1 class="my-4">Het Godenspel // The Extincticator</h1>

        <p>Het godenspel is een opera-game-installatie over leven, overleven, veerkracht en heldendom. Neem plaats aan tafel en speel mee voor de laatste overlevenden van een wereldse overstroming. Gebaseerd op Wagners grootse opera &lsquo;De Ring&rsquo;, bepalen de deelnemers samen wie mee mag op de boot en wie niet.<br>
        Hero&iuml;sche en persoonlijke heldenverhalen nodigen uit tot goede gesprekken over goede gesprekken over de belangrijkste eigenschappen van de mens, voor nu en in de toekomst.<br>
        Aan het slot van het spel wordt de Gouden Loki uitgereikt, gebaseerd op de onruststoker in het verhaal. Welke impact gaat de Loki hebben op de spelregels?</p>

        <i>Praktische info:</i>

        <p>Het Godenspel wordt gespeeld in een cirkel waarin de deelnemers op 1,5 meter afstand van elkaar zitten ( Covid-proof).<br>
        Groepsgrootte is minimaal 6 en maximaal 11 mensen.<br>
        Het spel wordt begeleid door een zanger/acteur en duurt 90 min.</p>

        <i>Credits:</i>

        <p>Concept / regie : Arlon Luijten<br>
        Game Design : Arlon Luijten en Joris Albeda<br>
        Systeemontwikkelaar: Joris Albeda<br>
        Spel/zang: Yorick Heerkens<br>
        Zang: Els Mondelaers<br>
        Muziek : Ben van Bueren<br>
        Dramaturgie: Cecile Brommer<br>
        Vormgeving : Bart Visser<br>
        Bouw Ring : Jan Neggers<br>
        Bouw Extincticator: Quinten de Smedt</p>

        <p>Het Godenspel is een productie van Little Wotan:<br>
        <a href="https:///www.littlewotan.nl">www.littlewotan.nl</a></p>
    </div>

    @include('includes.footer')
@stop
