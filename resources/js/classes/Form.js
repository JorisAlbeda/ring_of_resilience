import Errors from './Errors';

class Form {
	constructor(data) {
		this.originalData = data;
		
		for(let field in data) {
			this[field] = data[field];
		}
		
		this.errors = new Errors();
	}
	
	reset() {
		for(let field in this.originalData) {
			this[field] = '';
		}
		
		this.errors.clear();
	}
	
	post(url) {
		return this.submit('post', url);
	}
	
	data() {			
		let res = {};
		for (let property in this.originalData) {
			res[property] = this[property];
		}
		
		return res;
	}
	
	save() {
		let res = this.data();
		this.reset();
		return res;
	}
	
	submit(method, url) {
		return new Promise((resolve, reject) => {
			axios[method](url, this.data())
				.then(response => {
					this.onSuccess(response.data);
					
					resolve();
				})
				.catch(error => {
					this.onFail(error.response.data);
			
					reject(error.response.data);
				})
		});
	}
	
	onSuccess(data) {
		alert(data.message);
		
		this.reset();
	}
	
	onFail(errors) {
		if(errors) {
			this.errors.record(errors.errors);
		}
	}
}

export default Form;