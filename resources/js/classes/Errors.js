class Errors {
	constructor() {
		this.errors = {};
	}
	
	get(field) {
		if(this.errors[field]) {
			return this.errors[field][0];
		}
	}
	
	has(field) {
		return this.errors.hasOwnProperty(field);
	}
	
	any() {
		return Object.keys(this.errors).length > 0;
	}
	
	record(newErrors) {
		this.errors = newErrors;
	}
	
	clear(field) {
		if(field) {
			delete this.errors[field];
		} else {
			this.errors = {};
		}
	}
}

export default Errors;