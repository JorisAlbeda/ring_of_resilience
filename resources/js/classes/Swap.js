class Swap {
	
	constructor(attributes) {
		for(let field in attributes) {
			this[field] = attributes[field];
		};		
	}
	
	approve() {
		this.isApproved = true;
	}
	
	deny() {
		this.isApproved = false;
	}
}

export default Swap;