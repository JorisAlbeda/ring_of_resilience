class Player {
	constructor(attributes) {
		
		for(let field in attributes) {
			this[field] = attributes[field];
		}
		
		this.values = [];
		this.answers = [];
	}
	
	selectAnswer(answer) {
		this.answers.push(answer);
		
		for(let value of answer.values) {
			this.values.push(value);
		}		
	}
	
	hasValue(value) {
		return this.values.some(ownValue => ownValue.name == value.name);
	}
}

export default Player;