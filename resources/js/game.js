import router from './routes';
import Vuex from 'vuex';
import createStore from './store';
import Header from './components/Header';

let store = createStore(data);

var app = new Vue({
	router,
	store,
	components: {'ring-header': Header}
}).$mount('#app');