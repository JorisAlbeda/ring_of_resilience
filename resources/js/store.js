import Vue from 'vue';
import Vuex from 'vuex';
import Player from './classes/Player';

Vue.use(Vuex);

function createStore(data) {
	//let testPlayer = new Player({"name" : "test"});
	//let testSwap = {player: {"name" : "test", "index": 0}, oldValue: data.values[0], newValue: data.values[10], is_approved: undefined, is_applied: false};

	return new Vuex.Store({
		state: {
			players: [],
			judges: [],
			swaps: [],

            session_id: data.session_id,

			questions: data.questions,
			values: data.values,

			loadedMusicCount: 0,
			musicCount: 0
		},

		mutations: {
			addPlayer(state, player) {
				state.players.push(player);
			},

			addJudge(state, judge) {
				state.judges.push(judge);
			},

			addSwap(state, swap) {
				state.swaps.push(swap);
			},

			approveLatestSwap(state) {
				state.swaps[state.swaps.length - 1].is_approved = true;
			},

			denyLatestSwap(state) {
				state.swaps[state.swaps.length - 1].is_approved = false;
			},

			applySwap(state) {
				let swap = state.swaps[state.swaps.length - 1];
				let oldValuePosition = swap.oldValue.position;
				let newValuePosition = swap.newValue.position;

				if(!swap.is_applied && swap.oldValue.position > 0) {
					for(let value of state.values) {
						if(value.position > oldValuePosition) {
							value.position--;
						} else if(value.id == swap.oldValue.id) {
							value.position = newValuePosition;
						} else if(value.id == swap.newValue.id) {
							value.position = 8;
						}
					}
				}

				swap.is_applied = true;
			},

			setMusicCount(state, count) {
				state.musicCount = count;
			},

			incrementLoadedMusicCount(state) {
				state.loadedMusicCount++;
			},

            resetGame(state, sessionId) {
			    state.players = [];
			    state.judges = [];
			    state.swaps = [];

			    state.session_id = sessionId;
            }
		},

		getters: {
			getTopEight(state) {
				return state.values
					.filter(value => value.position > 0)
					.sort((value1, value2) => value1.position - value2.position);
			},

            newValueOptions(state) {
			    return state.values
                    .filter(value => value.position == null || value.position == 0)
                    .sort((value1, value2) => value1.name > value2.name ? 1 : -1);
            },

			getLatestSwap(state) {
				return state.swaps[state.swaps.length - 1];
			},

			isWelcome: (state, getters) => (player) => {
				return getters.getTopEight.every(value => player.hasValue(value));
			},

			getWelcomePlayers(state, getters) {
				return state.players.filter(player => getters.isWelcome(player));
			},
		}
	});
};

export default createStore;
