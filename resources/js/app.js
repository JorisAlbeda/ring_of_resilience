require('./bootstrap');
import router from './routes';
import Vuex from 'vuex';
import createStore from './store';
import Header from './components/Header';
import AudioPlayer from './components/AudioPlayer';
import VueHtmlToPaper from 'vue-html-to-paper';

let store = createStore(data);

//Vue.component('ring-audio-player', AudioPlayer, {'name': 'ring-audio-player'});

Vue.use(VueHtmlToPaper, {
	name: '_blank',
	specs: [
		'fullscreen=yes',
		'titlebar=no',
		'scrollbars=no',
	],
});

var app = new Vue({
	router,
	store,
	components: {
		'ring-header': Header,
		'audio-player': AudioPlayer
	},
	methods: {
		playMusic(musicName) {
			console.log('Playing ' + musicName);
			if(musicName == ''){
				this.$refs.audioPlayer.play();
			} else {
				this.$refs.audioPlayer.playMusic(musicName);
			}
		},

		play() {
			this.$refs.audioPlayer.play();
		},

		playSound(soundName) {
			this.$refs.audioPlayer.playSound(soundName);
		},

		pauseMusic() {
			this.$refs.audioPlayer.pause();
		},

		fadeToMusic(musicName, fadeSpeed, newVolume) {
			this.$refs.audioPlayer.fadeToMusic(musicName, fadeSpeed, newVolume);
		},
	}
}).$mount('#app');
