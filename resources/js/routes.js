import VueRouter from 'vue-router';

let routes = [
	{
		path: '/',
		component: require('./views/Home').default
	},
	{
		path: '/welcome',
		component: require('./views/Welcome').default
	},
	{
		path: '/create-players',
		component: require('./views/CreatePlayers').default
	},
	{
		path: '/questions',
		component: require('./views/Questions').default
	},
	{
		path: '/results-scene',
		component: require('./views/ResultsScene').default
	},
	{
		path: '/is-welcome',
		component: require('./views/IsWelcome').default
	},
	{
		path: '/swap-scene',
		component: require('./views/SwapScene').default
	},
	{
		path: '/create-swap',
		component: require('./views/CreateSwap').default
	},
	{
		path: '/finish-swap',
		component: require('./views/FinishSwap').default
	},
	{
		path: '/apply-swap',
		component: require('./views/ApplySwap').default
	},
	{
		path: '/revised-results',
		component: require('./views/RevisedResults').default
	},
	{
		path: '/swap-done',
		component: require('./views/SwapDone').default
	},
	{
		path: '/epilogue',
		component: require('./views/Epilogue').default
	},


	/**
	 * AUDIENCE
	 */
	{
		path: '/audience/finish-swap',
		component: require('./views/audience/FinishSwap').default
	}
];

export default new VueRouter({
	routes
});
